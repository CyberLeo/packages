# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=rsync
pkgver=3.2.7
pkgrel=0
pkgdesc="File transfer program to keep remote files in sync"
url="https://rsync.samba.org/"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="fakeroot"
makedepends="perl acl-dev attr-dev lz4-dev openssl-dev popt-dev zlib-dev
	zstd-dev"
subpackages="$pkgname-doc $pkgname-openrc rrsync::noarch"
source="https://download.samba.org/pub/$pkgname/src/$pkgname-$pkgver.tar.gz
	rsyncd.initd
	rsyncd.confd
	rsyncd.conf
	rsyncd.logrotate
	"

# secfixes:
#   3.2.7-r0:
#     - CVE-2022-29154
#   3.2.3-r0:
#     - CVE-2020-14387
#   3.1.3-r2:
#     - CVE-2016-9840
#     - CVE-2016-9841
#     - CVE-2016-9842
#     - CVE-2016-9843

build() {
	# Force IPv6 enabled, upstream bug https://bugzilla.samba.org/show_bug.cgi?id=10715
	CFLAGS="$CFLAGS -DINET6" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-acl-support \
		--enable-xattr-support \
		--with-included-zlib=no \
		--disable-xxhash
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -D -m 755 "$srcdir"/rsyncd.initd "$pkgdir"/etc/init.d/rsyncd
	install -D -m 644 "$srcdir"/rsyncd.conf "$pkgdir"/etc/rsyncd.conf
	install -D -m 644 "$srcdir"/rsyncd.confd "$pkgdir"/etc/conf.d/rsyncd
	install -D -m 644 "$srcdir"/rsyncd.logrotate "$pkgdir"/etc/logrotate.d/rsyncd

}

rrsync() {
	pkgdesc="Restricted rsync, restricts rsync to a subdir declared in .ssh/authorized_keys"
	depends="rsync perl"

	install -D -m 755 "$builddir"/support/rrsync "$subpkgdir"/usr/bin/rrsync
}

sha512sums="c2afba11a352fd88133f9e96e19d6df80eb864450c83eced13a7faa23df947bccf2ef093f2101df6ee30abff4cbbd39ac802e9aa5f726e42c9caff274fad8377  rsync-3.2.7.tar.gz
638d87c9a753b35044f6321ccd09d2c0addaab3c52c40863eb6905905576b5268bec67b496df81225528c9e39fbd92e9225d7b3037ab1fda78508d452c78158f  rsyncd.initd
c7527e289c81bee5e4c14b890817cdb47d14f0d26dd8dcdcbe85c7199cf27c57a0b679bdd1b115bfe00de77b52709cc5d97522a47f63c1bb5104f4a7220c9961  rsyncd.confd
3db8a2b364fc89132af6143af90513deb6be3a78c8180d47c969e33cb5edde9db88aad27758a6911f93781e3c9846aeadc80fffc761c355d6a28358853156b62  rsyncd.conf
b8d6c0bb467a5c963317dc55478d2c10874564cd264d943d4a42037e2fce134fe001fabc92af5c6b5775e84dc310b1c8da147afaa61c99e5663c36580d8651a5  rsyncd.logrotate"
