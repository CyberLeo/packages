# Maintainer: Zach van Rijn <me@zv.io>
pkgname=libtool
pkgver=2.4.7
pkgrel=0
pkgdesc="A generic library support script"
arch="all"
license="GPL-2.0+"
url="http://www.gnu.org/software/libtool"
depends="bash libltdl"
makedepends="m4 help2man"
checkdepends="autoconf automake gzip"
subpackages="libltdl $pkgname-doc"
options="libtool"
source="https://ftp.gnu.org/gnu/$pkgname/${pkgname}-${pkgver}.tar.gz
	libtool-fix-cross-compile.patch
	fix-test-grep-expression.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		lt_cv_shlibpath_overrides_runpath=yes
	make
}

check() {
	# Test 70 and 116 are known to fail
	# Test 169 repeats the entire test suite with shorter max_cmd_len
	make check TESTSUITEFLAGS="1-69 71-115 117-168"
}

package() {
	make DESTDIR="$pkgdir" install
}

libltdl() {
	pkgdesc="Runtime libraries for GNU Libtool Dynamic Module Loader"
	license="LGPL-2.1+"
	depends=
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir/usr/lib/libltdl.so"* "$subpkgdir/usr/lib/"
}

sha512sums="27acef46d9eb67203d708b57d80b853f76fa4b9c2720ff36ec161e6cdf702249e7982214ddf60bae75511aa79bc7d92aa27e3eab7ef9c0f5c040e8e42e76a385  libtool-2.4.7.tar.gz
aa14cadd70a633520249fa3a6a4c45b309a980380e62dd5982b3ae842c478a77401ad809297fceebd167c167b0a19f380e6a6c295bc75d1029d56d3bf262b209  libtool-fix-cross-compile.patch
57d47955bd3d51c5df2d62ba4773687e9111bfc155312eaa10d84e273845e255c2e62a1ceb01e8ce35617cb702f116fc223121dbb69955f5141c0185fbf4b6b4  fix-test-grep-expression.patch"
