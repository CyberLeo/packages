# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=utmps
pkgver=0.1.2.2
pkgrel=0
pkgdesc="A secure utmp/wtmp implementation"
url="https://skarnet.org/software/utmps/"
arch="all"
options="!check"  # No test suite
license="ISC"
depends="execline s6"
_skalibs_version=2.14
makedepends="skalibs-dev>=$_skalibs_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc $pkgname-openrc"
install="$pkgname.post-upgrade"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz
	utmpd.run
	wtmpd.run
	btmpd.run
	utmps.initd"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib \
		--enable-libc-includes
	make
}

package() {
	make DESTDIR="$pkgdir" install
	runimage="$pkgdir/etc/s6-linux-init/current/run-image"
        mkdir -p -m 0755 "$runimage/utmps"
	chown utmp:utmp "$runimage/utmps"
	install -D -m755 "$srcdir/utmps.initd" "$pkgdir/etc/init.d/utmps"
}

openrc() {
	rldir="$subpkgdir"/etc/runlevels/boot
	subrunimage="$subpkgdir/etc/s6-linux-init/current/run-image"
	default_openrc
	mkdir -p -m 0755 "$rldir" "$subpkgdir/var/log/wtmpd" "$subrunimage/service/utmpd" "$subrunimage/service/wtmpd" "$subrunimage/service/btmpd"
        cp -f "$srcdir/utmpd.run" "$subrunimage/service/utmpd/run"
	echo 3 > "$subrunimage/service/utmpd/notification-fd"
	touch "$subrunimage/service/utmpd/down"
        cp -f "$srcdir/wtmpd.run" "$subrunimage/service/wtmpd/run"
	echo 3 > "$subrunimage/service/wtmpd/notification-fd"
	touch "$subrunimage/service/wtmpd/down"
        cp -f "$srcdir/btmpd.run" "$subrunimage/service/btmpd/run"
	echo 3 > "$subrunimage/service/btmpd/notification-fd"
	touch "$subrunimage/service/btmpd/down"
        chmod 0755 "$subrunimage/service/utmpd/run" "$subrunimage/service/wtmpd/run" "$subrunimage/service/btmpd/run"
	chown utmp:utmp "$subpkgdir/var/log/wtmpd"
	ln -s wtmpd/wtmp "$subpkgdir/var/log/wtmp"
	ln -s wtmpd/btmp "$subpkgdir/var/log/btmp"
	ln -s ../../init.d/utmps "$rldir/utmps"
}

libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}

dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        install_if="dev $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr"
        mv "$pkgdir/usr/lib" "$pkgdir/usr/include" "$subpkgdir/usr/"
}

libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}

doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=""
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="57a73658ecb947af9dfad7a5e2931660ad1b8fa61d36c803c373e8aba13e9afa8398c1522765f5ea2b5df87d942cea17062faf30f589afa6acc744ff3ae4a409  utmps-0.1.2.2.tar.gz
0ec30284c64c6ea9f25142c5f4a643bd48b137fe85781b650104f5137ffa4dfc35ca7be3e41e3acd3403ebe1d8c5378073afa4e2f3607d3d794fcd9f98ed51c4  utmpd.run
9e875a5cd37be531320a8e582afed2c980dd0a1bdfc2f6f3d826d5e5389fc6ab93f973ed1506edb23f4c73cf24a2357aefe856148eaacff86c2aafe376c575e2  wtmpd.run
503bdbb3d244243934b9b4e3deea0bf92a95f88417c822ad9cf6202584d4724d5e182a0d88d7f09069e435a8a97230b85d2b264736c85c893da193fd5ec34c71  btmpd.run
89fb24ab5759b3a1162044895efbe01c7a17d505a29df0cd7141654783b7ada8e934a1ce5981218af41812cfb517263bbd608948102af40b7a41a843b7aaf6ca  utmps.initd"
