# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=weechat
pkgver=3.8
pkgrel=0
pkgdesc="Fast, light, extensible ncurses-based chat client"
url="https://www.weechat.org"
arch="all"
options="!check"  # requires all plugins be built.
license="GPL-3.0+"
depends=""
depends_dev="cmake aspell-dev curl-dev gnutls-dev libgcrypt-dev lua5.3-dev
	ncurses-dev perl-dev python3-dev ruby-dev tcl-dev zlib-dev guile-dev
	tk-dev zstd-dev"
checkdepends="cpputest"
makedepends="$depends_dev asciidoctor"
subpackages="$pkgname-dev $pkgname-spell:_plugin $pkgname-lua:_plugin
	$pkgname-perl:_plugin $pkgname-python:_plugin $pkgname-ruby:_plugin
	$pkgname-tcl:_plugin $pkgname-guile:_plugin $pkgname-doc"
source="https://www.weechat.org/files/src/$pkgname-$pkgver.tar.gz"

# secfixes:
#   3.4-r0:
#     - CVE-2021-40516
#   1.7.1-r0:
#     - CVE-2017-8073
#   1.9.1-r0:
#     - CVE-2017-14727
#   2.7.1-r0:
#     - CVE-2020-8955

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_JAVASCRIPT=OFF \
		-DENABLE_MAN=ON \
		-DENABLE_NLS=OFF \
		-DENABLE_PHP=OFF \
		-Bbuild .
	make -C build
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir/" -C build install
}

_plugin() {
	_name="${subpkgname#*-}"
	_dir=usr/lib/weechat/plugins
	pkgdesc="WeeChat $_name plugin"
	depends="weechat"
	if [ "$_name" = spell ]; then
		provides="$pkgname-aspell=$pkgver-r$pkgrel"
	fi

	mkdir -p "$subpkgdir"/$_dir
	mv "$pkgdir"/$_dir/${_name}.so "$subpkgdir"/$_dir
}

sha512sums="1c176280380d735ae609341272526b1bb8707b98c6715c69ac298f79e290ebb5544f7abc4b3f5c751168a99e78d8919b135e8557adea50af6e467bf7ee340ca6  weechat-3.8.tar.gz"
