# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khtml
pkgver=5.94.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
url="https://konqueror.org/"
arch="all"
options="!check"  # Tests require X11.
license="LGPL-2.1+ AND LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev kcodecs-dev ki18n-dev kparts-dev ktextwidgets-dev
	kjs-dev"
makedepends="$depends_dev cmake extra-cmake-modules giflib-dev gperf kio-dev
	karchive-dev kglobalaccel-dev kiconthemes-dev knotifications-dev
	kwallet-dev libjpeg-turbo-dev libpng-dev libx11-dev openssl-dev
	phonon-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="46ba7aa0f29868773db54e632aeeefdd6a92f9b6ac55f9bebb5d903f04f7a41fe9afae52fe43cd23c9f512382042e8c709f5b7897d8ed98e73e710188d1be568  khtml-5.94.0.tar.xz"
