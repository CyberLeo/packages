# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=nmap
pkgver=7.92
pkgrel=1
pkgdesc="A network exploration tool and security/port scanner"
url="https://nmap.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="linux-headers openssl-dev libpcap-dev pcre-dev zlib-dev
	libssh2-dev lua5.3-dev"
subpackages="
	$pkgname-doc
	$pkgname-scripts::noarch
	$pkgname-nselibs::noarch
	$pkgname-nping
	$pkgname-ncat
	$pkgname-ncat-doc:ncat_doc
	netcat::noarch"
source="https://nmap.org/dist/$pkgname-$pkgver.tar.bz2
	disable-broken-tests.patch
	"

# secfixes:
#   7.80-r0:
#     - CVE-2018-15173

build() {
        # zenmap and ndiff require python 2
	export CFLAGS=-g3
	export CPPFLAGS=-g3
	export CXXFLAGS=-g3
	LDFLAGS="$LDFLAGS -L/usr/lib/lua5.3" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-zenmap \
		--without-ndiff \
		--with-openssl=yes \
		--with-libpcap=yes \
		--with-libpcre=yes \
		--with-libz=yes \
		--with-libssh2=yes \
		--with-liblua=yes
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	install -Dm644 LICENSE ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}

scripts() {
	depends="$pkgname-nselibs"
	pkgdesc="$pkgdesc (scripts)"

	mkdir -p "$subpkgdir"/usr/share/nmap/
	mv "$pkgdir"/usr/share/nmap/scripts \
		"$subpkgdir"/usr/share/nmap/
}

nselibs() {
	pkgdesc="$pkgdesc (nselibs)"

	mkdir -p "$subpkgdir"/usr/share/nmap/
	mv "$pkgdir"/usr/share/nmap/nse_main.lua \
		"$pkgdir"/usr/share/nmap/nselib \
		"$subpkgdir"/usr/share/nmap/
}

ncat() {
	pkgdesc="$pkgdesc (ncat tool)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ncat "$subpkgdir"/usr/bin
}

ncat_doc() {
        pkgdesc="ncat utility (docs)"
	install_if="nmap-ncat=$pkgver-r$pkgrel docs"
	mkdir -p "$subpkgdir"/usr/share/man/man1
	mv "${pkgdir}-doc"/usr/share/man/man1/ncat.1.gz \
		"$subpkgdir"/usr/share/man/man1
}

nping() {
	pkgdesc="$pkgdesc (nping tool)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/nping "$subpkgdir"/usr/bin
}

netcat() {
	pkgdesc="Symlinks for netcat and nc to ncat"
	depends="$pkgname-ncat"
	mkdir -p "$subpkgdir"/usr/bin
	ln -s ncat "$subpkgdir"/usr/bin/netcat
	ln -s ncat "$subpkgdir"/usr/bin/nc
}

sha512sums="7828367f9dc76ff4d1e8c821260e565fb0c3cb6aba0473d24759133a3006cdf2cb087574f0dd7d2ba47a63754ba4f72e0b78cdae1333a58f05c41d428b56ad59  nmap-7.92.tar.bz2
0f42b7d5e65ec82d7ed3d385d2f71c3234df7058a728ae602e7e557624faeea8f72832bc7ef861e337c407ab200a9e2ecc202c526bfd10cb6fd64262c3cc4a6d  disable-broken-tests.patch"
