# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=xscreensaver
pkgver=6.06
pkgrel=0
pkgdesc="X Screensaver suite"
url="https://www.jwz.org/xscreensaver/"
arch="all"
options="!check suid"  # No test suite.
license="MIT"
depends=""
makedepends="bc gtk+3.0-dev intltool libice-dev libjpeg-turbo-dev libx11-dev
	libxft-dev libxi-dev libxinerama-dev libxml2-dev libxml2-utils glu-dev
	libxrandr-dev libxt-dev linux-pam-dev mesa-dev mesa-glapi xorgproto-dev
	desktop-file-utils elogind-dev xdg-utils"
subpackages="$pkgname-doc $pkgname-elogind"
source="https://www.jwz.org/xscreensaver/xscreensaver-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-pam \
		--with-shadow \
		--with-elogind
	make
}

package() {
	mkdir -p "$pkgdir"/etc/pam.d
	make install_prefix="$pkgdir" install
}

elogind() {
	pkgdesc="$pkgdesc (elogind integration)"
	install_if="$pkgname=$pkgver-r$pkgrel elogind"
	mkdir -p "$subpkgdir"/usr/libexec/$pkgname
	mv "$pkgdir"/usr/libexec/$pkgname/xscreensaver-systemd \
		"$subpkgdir"/usr/libexec/$pkgname/
}

sha512sums="988e30d422ef985ac348c275e098ddfe1ee034a2e916c91690ee2836c908801c1e017e22d828aca981b0f8bfc5491cd83ab7c45aabc155ba5013df8b149cbcb5  xscreensaver-6.06.tar.gz"
