# Contributor: Pedro Filipe <xpecex@outlook.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=mosquitto
pkgver=2.0.15
pkgrel=0
pkgdesc="An Open Source MQTT Broker"
url="https://mosquitto.org/"
arch="all"
license="EPL-1.0 AND EDL-1.0 AND MIT AND BSD-3-Clause"
depends=""
checkdepends="python3 ca-certificates cunit-dev"
makedepends="c-ares-dev openssl-dev util-linux-dev cjson-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-openrc
	$pkgname-clients"
install="$pkgname.pre-install"
source="http://mosquitto.org/files/source/$pkgname-$pkgver.tar.gz
	mosquitto.initd
	openrc-conf.patch
	endian.patch
	no-persist_read_test.patch
	"

# secfixes:
#   1.6.6-r0:
#     - CVE-2019-11779
#   1.5.6-r0:
#     - CVE-2018-12546
#     - CVE-2018-12550
#     - CVE-2018-12551
#   1.5.3-r0:
#     - CVE-2018-12543
#   1.4.15-r0:
#     - CVE-2017-7652
#     - CVE-2017-7651
#   1.4.13-r0:
#     - CVE-2017-9868
#   1.4.12-r0:
#     - CVE-2017-7650

build() {
	make \
		WITH_ADNS=no \
		WITH_MEMORY_TRACKING=no \
		WITH_SRV=yes \
		prefix=/usr
}

check() {
	# Explicitly disable tests, instead of enabling only good ones.
	# 08-ssl-connect-cert-auth-without-py: #687
	# 06-bridge-reconnect-local-out.py:
	#   https://www.eclipse.org/lists/mosquitto-dev/msg02687.html
	#   (fix commit is not easily backported due to other churn)
	# Other two: https://github.com/eclipse/mosquitto/issues/2550
	sed -i test/broker/test.py \
		-e '/08-ssl-connect-cert-auth-without.py/d' \
		-e '/06-bridge-b2br-late-connection-retain.py/d' \
		-e '/06-bridge-reconnect-local-out.py/d' \
		-e '/11-pub-props.py/d' \
		;
	sed -i test/lib/test.py \
		-e '/02-subscribe-qos1.py/d' \
		;
	sed -i test/client/Makefile \
		-e '/test.sh/d' \
		;
	# -j1 to avoid TLS errors on at least pmmx due to race.
	make -j1 ptest
}

package() {
	make prefix=/usr DESTDIR="$pkgdir" install

	# C++ bindings are deprecated, so don't install them.
	make -C lib/cpp prefix=/usr DESTDIR="$pkgdir" uninstall
	rm "$pkgdir"/usr/lib/pkgconfig/libmosquittopp.pc

	mv "$pkgdir"/usr/sbin/mosquitto "$pkgdir"/usr/bin
	mv "$pkgdir"/etc/mosquitto/mosquitto.conf.example \
		"$pkgdir"/etc/mosquitto/mosquitto.conf

	install -Dm755 "$srcdir"/mosquitto.initd \
		"$pkgdir"/etc/init.d/mosquitto
}

clients() {
	pkgdesc="Mosquitto command line MQTT clients"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/mosquitto_[ps]ub "$subpkgdir"/usr/bin/
}

sha512sums="8a0c58222ea0b9304e7f096f6d13965478072baf1d1c9bc931b32a33f5ea8196c2dd54bde8d83fec7ed9cd413cb5fb115ce0a6701ca20664df52fdd7ca63ddaf  mosquitto-2.0.15.tar.gz
681fddc737b3ef3e6c052f0c854a730df290352640a18a63e23ef83d14c425558aff87805d1eb95e44de101b5df48872173af9f5414464ffa8cf727ea2c0491e  mosquitto.initd
b07f9bec2751ab32c43f53e74d8fca18dbf2d7ce7f8fab562dbcf75de19609ba6219d735ac504697404e0ed36613a14074e3a19e735297195877798f778d337a  openrc-conf.patch
89f738f77732500bc2aff3086b891c1e537510e756baf0ac92c1462c2ef7e47cdc62536d90174e6b6c80637199e463609723f9915999c0b95ce878ebbed739a1  endian.patch
b173f11e898bf2d882b7d833f99c863dc1fd14ee6f55484c29dafd067fa1af27ab85864df674c0a042547c5c132b80123d159f10e7bdab316a0292461b91fafd  no-persist_read_test.patch"
