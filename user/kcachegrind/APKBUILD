# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcachegrind
pkgver=22.04.2
pkgrel=0
pkgdesc="Profile data visualisation tool and call graph viewer"
url="https://kcachegrind.github.io/html/Home.html"
arch="all"
license="GPL-2.0-only"
depends="binutils graphviz"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	karchive-dev kconfig-dev kcoreaddons-dev kdoctools-dev ki18n-dev kio-dev
	kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcachegrind-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="66f70496776b799623567a1b6ba23c0d07fc6a6ababa5500f4890d796b926c5cf09bc910ccf27b5f8487a14b6cd09c403df13d80501fc96061c02286dd2558c9  kcachegrind-22.04.2.tar.xz"
