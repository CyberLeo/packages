# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kwayland-server
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE Wayland server component"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires D-Bus.
license="LGPL-2.1+ AND LGPL-2.1-only AND LGPL-3.0-only AND BSD-3-Clause AND MIT AND MIT-CMU"
depends="plasma-wayland-protocols"
docdepends="kwayland-doc"
makedepends="cmake extra-cmake-modules kwayland-dev mesa-dev qt5-qtbase-dev
	qt5-qtwayland-dev qt5-qtwayland-tools wayland-dev wayland-protocols
	qt5-qttools-dev doxygen $docdepends"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/plasma/$pkgver/kwayland-server-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="72159618a22233b9b49cab8ee1a804f629763e903a2e711c8c23f990544d79b5d27e768c33f8d844c916e41a6d3a2af5ddcab69d0448569adc247e0f737975f5  kwayland-server-5.24.5.tar.xz"
