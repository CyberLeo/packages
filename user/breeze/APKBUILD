# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=breeze
pkgver=5.24.5
pkgrel=0
pkgdesc="Default KDE Plasma 5 style"
url="https://www.kde.org/"
arch="all"
license="(GPL-2.0-only AND GPL-3.0-only) AND MIT AND GPL-2.0+"
depends=""
depends_dev="extra-cmake-modules"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcmutils-dev
	kconfigwidgets-dev kcoreaddons-dev kdecoration-dev kguiaddons-dev
	ki18n-dev kpackage-dev frameworkintegration-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="25e5744589130f7d339c727e17e662a28bdecc3f0598b9683735ccdfd2dcdeada46a49baa5564a7cf7d59a3f6ac34802ae2172857e704941d64cf879b3583df0  breeze-5.24.5.tar.xz"
