# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdepim
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE PIM runtime library"
url="https://kontact.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev qt5-qttools-dev cmake extra-cmake-modules boost-dev
	kauth-dev kcalendarcore-dev kcmutils-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcontacts-dev kcoreaddons-dev ki18n-dev kio-dev
	kitemmodels-dev kitemviews-dev kjobwidgets-dev kldap-dev kmime-dev
	kservice-dev kwallet-dev kwidgetsaddons-dev kwindowsystem-dev
	kxmlgui-dev solid-dev

	akonadi-dev akonadi-contacts-dev akonadi-mime-dev akonadi-search-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdepim-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="547fc45bfdad33db4cfde8cf162c818a1dce4959d8c7f82fa59566e0ad1c536e0de037b00561318de5ff30d7736420b25715b6032c2f321ad00291e28a5860b0  libkdepim-22.04.2.tar.xz"
