# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rsibreak
pkgver=0.12.15
pkgrel=0
pkgdesc="Helps you avoid wrist injury by telling you when to stop for a rest"
url="https://www.kde.org/applications/utilities/rsibreak/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev kcrash-dev
	kconfigwidgets-dev kdbusaddons-dev kdoctools-dev kiconthemes-dev
	ki18n-dev kidletime-dev knotifications-dev knotifyconfig-dev
	ktextwidgets-dev kxmlgui-dev kwindowsystem-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/rsibreak/0.12/rsibreak-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8178387716b9d611a33ce72070bae1c3d55ad098100a701fcae9f40a8db4bb29eda364a799223a6fea5671e44e0a5a41aa2b6dff2fb50a832221e468c17de87b  rsibreak-0.12.15.tar.xz"
