# Maintainer: Zach van Rijn <me@zv.io>
pkgname=gtest
pkgver=1.10.0
pkgrel=1
pkgdesc="C++ testing framework based on xUnit (like JUnit)"
url="https://github.com/google/googletest"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends="libgcc bash"
depends_dev="python3 cmake"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/google/googletest/archive/release-$pkgver.tar.gz"
builddir="$srcdir"/googletest-release-${pkgver}

build() {
	rm -rf build
	mkdir build
	cd build

	cmake -DBUILD_SHARED_LIBS=ON \
	      -DCMAKE_SKIP_RPATH=ON ../
	make
}

package() {
	for k in mock test; do
		for dir in usr/lib usr/include/g${k}/internal/custom usr/share/licenses/g${k}\
				usr/src/g${k}/cmake usr/src/g${k}/src; do
			install -d -m 0755 "$pkgdir"/"$dir"
		done
		install -m 0644 build/lib/libg${k}*.so "$pkgdir"/usr/lib

		install -m 0644 google${k}/include/g${k}/*.h "$pkgdir"/usr/include/g${k}
		install -m 0644 google${k}/include/g${k}/internal/*.h \
			"$pkgdir"/usr/include/g${k}/internal/
		install -m 0644 google${k}/include/g${k}/internal/custom/*.h \
			"$pkgdir"/usr/include/g${k}/internal/custom/
		install -m 0644 google${k}/LICENSE "$pkgdir"/usr/share/licenses/g${k}/
		install -m 0644 google${k}/CMakeLists.txt "$pkgdir"/usr/src/g${k}/
		install -m 0644 google${k}/cmake/* "$pkgdir"/usr/src/g${k}/cmake/
	done
}

sha512sums="bd52abe938c3722adc2347afad52ea3a17ecc76730d8d16b065e165bc7477d762bce0997a427131866a89f1001e3f3315198204ffa5d643a9355f1f4d0d7b1a9  release-1.10.0.tar.gz"
