# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalendarcore
pkgver=5.94.0
pkgrel=0
pkgdesc="Library for managing a calendar of events"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
checkdepends="tzdata"
makedepends="$depends_dev cmake extra-cmake-modules libical-dev
	doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
replaces="kcalcore"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	x11_tests="testalarm testattachment testattendee testcalfilter
	testconference testcustomproperties testdateserialization testduration
	testevent testincidence testexception testfilestorage testfreebusy
	testindicdencerelation testicalformat testidentical testjournal
	testmemorycalendar testperiod testfreebusyperiod testperson
	testrecurtodo teststartdatetimesfordate testtodo testtimesininterval
	testcreateddatecompat testrecurrenceexception testoccurrenceiterator
	testreadrecurrenceid testcalendarobserver"

	# https://invent.kde.org/frameworks/kcalendarcore/-/issues/3
	failing_tests="Compat-AppleICal Compat-MSExchange"

	skipline="(testicaltimezones"
	for tname in $failing_tests $x11_tests; do
		skipline="$skipline|$tname"
	done
	skipline="$skipline)"
	echo $skipline

	TZ=UTC CTEST_PARALLEL_JOBS=${JOBS} CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipline"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e9747a1965ca78d6112276cbd501cf00494fe7ba777a24a92e7f0f1b605770f924bca2d1b7e30b69b19051f2c823d71faa37ae4768857d6adb38d7c0dfd9a32c  kcalendarcore-5.94.0.tar.xz"
