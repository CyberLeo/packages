# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=gtksourceview4
_pkgreal=${pkgname%4}
pkgver=4.8.3
pkgrel=0
pkgdesc="GNOME library for syntax-highlighting code"
url="https://www.gnome.org/"
arch="all"
options="!check" # majority of tests fail with SIGTRAP
license="LGPL-2.1+ AND LGPL-2.0+ AND (Apache-2.0 or MIT~unspecified and/or LGPL-2.1+)"
depends=""
makedepends="glib-dev gobject-introspection-dev gtk-doc gtk+3.0-dev libxml2-dev meson ninja vala-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/$_pkgreal/${pkgver%.*}/$_pkgreal-$pkgver.tar.xz"
builddir="$srcdir/$_pkgreal-$pkgver"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		--wrap-mode=nofallback \
		-Dgtk_doc=true \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="406fae70301336eb31a05474db28b792212fa82db5acc209322587838655416955f8a91a6e081a1d0a22330e199d1815b5dd9d3299bfc9894e127b38ed6be418  gtksourceview-4.8.3.tar.xz"
