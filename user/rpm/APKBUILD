# Contributor: Nathan <ndowens@artixlinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=rpm
pkgver=4.16.0
pkgrel=0
pkgdesc="Redhat Package Manager"
url="https://www.rpm.org"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends=""
makedepends="acl-dev autoconf automake binutils-dev bzip2-dev file-dev
	graphviz libarchive-dev libcap-dev libgcrypt-dev
	libtool popt-dev sqlite-dev xz-dev zlib-dev zstd-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://ftp.rpm.org/releases/$pkgname-${pkgver%.*}.x/$pkgname-$pkgver.tar.bz2
	musl.patch
	include-fcntl.patch"

prepare() {
	# Use sqlite db
	sed -ie "/_db_backend/ s/ bdb/ sqlite/g" macros.in
	default_prepare
	autoreconf -fi
}

# Py dependencies isn't currently in repo, so disable for now
build() {
	LIBS="$LIBS -lintl" \
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-cap \
		--with-acl \
		--without-lua \
		--disable-python \
		--enable-zstd \
		--enable-sqlite=yes \
		--enable-bdb_ro=yes \
		--enable-bdb=no \
		--with-crypto=libgcrypt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="177119c3ac3d48980db55bb4ba0fdbb2a911968e5efc690bfa8cc343f850fc90531cc0dee6dd8e45d2b14f0d951ced35bd8893d24011b7f270745d281ddf4e3d  rpm-4.16.0.tar.bz2
212a4265abc8d002e16bed106b8b773cf65564f95e6074bc1378c4745420202a476373b49b660bdfe82cc2470c35fff4f184168a698abfa2a4bf30c8f91e64ad  musl.patch
6424005c78aaebcd3565debbdc1ca14fb16ef8f4aa79748eca3403115a31c77afbb8929add1a8450afbd0496e303c915c6ad6d60cde41a89caf553a10256ace5  include-fcntl.patch"
