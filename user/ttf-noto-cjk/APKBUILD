# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=ttf-noto-cjk
pkgver=20170601
pkgrel=1
pkgdesc="Noto fonts for Chinese, Japanese, and Korean"
url="https://www.google.com/get/noto/help/cjk/"
arch="noarch"
options="!check"  # how do you test a font?
license="OFL-1.1"
depends="fontconfig"
makedepends=""
# * The archive is taken from https://github.com/googlei18n/noto-cjk/releases
#   with only the Noto*CJK-*.ttc files kept. These are the so-called
#   "OpenType/CFF Collection (OTC)" files, which combine the Traditional
#   Chinese, Simplified Chinese, Japanese, and Korean glyphs into single files.
#   There are 7 weight variants of NotoSansCJK (which includes Noto Sans CJK
#   and Noto Sans Mono CJK), and 7 weight variants of NotoSerifCJK, for a total
#   of 14 files.
# * Adapted fontconfig configuration from:
#   https://github.com/bohoomil/fontconfig-ultimate/tree/master/fontconfig_patches/fonts-settings
source="https://dev.sick.bike/$pkgname-$pkgver.tar.xz
	90-ttf-noto-cjk.conf
	"

package() {
	mkdir -p "$pkgdir"/usr/share/fonts/"$pkgname" \
		"$pkgdir"/etc/fonts/conf.d \
		"$pkgdir"/etc/fonts/conf.avail

	cp "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/"$pkgname"

	cp "$srcdir"/90-ttf-noto-cjk.conf "$pkgdir"/etc/fonts/conf.avail
	cd "$pkgdir"/etc/fonts/conf.d
	ln -sf /etc/fonts/conf.avail/90-ttf-noto-cjk.conf
}

sha512sums="5ac6740ef2cc4a4f1f59304c617b2268e220f3d7d72430184506cb1ccd86cebea4c4f5366a7285ebd2993e6a59557a0396badaeb6239f51f15db1c5d28b5ae03  ttf-noto-cjk-20170601.tar.xz
ea74f9b289b85926b973d15124fe863e0a537bb7ef5cf523f7e960b03bfbd53c86be01ac8062a4d71f77d943ae4695a91a8168c8351b5738081663639ff4982c  90-ttf-noto-cjk.conf"
