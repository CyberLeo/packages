# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knetwalk
pkgver=22.04.2
pkgrel=0
pkgdesc="Build up a computer network by placing the wires correctly"
url="https://www.kde.org/applications/games/knetwalk/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev
	kdbusaddons-dev ki18n-dev ktextwidgets-dev kwidgetsaddons-dev
	kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f1f8fe24c95140693c2cdcd51605a8a0c7f989a61752c992f0bdc245e05cfc67a1f4b991c0b31e613262416d732bff1cc91fe086937fdc1968d32b652abd7427  knetwalk-22.04.2.tar.xz"
