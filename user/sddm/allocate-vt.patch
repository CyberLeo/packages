From f0b6dec66a6fc8cc6e21da57f81190843b483f76 Mon Sep 17 00:00:00 2001
From: Pier Luigi Fiorini <pierluigi.fiorini@liri.io>
Date: Sun, 28 Feb 2021 12:07:33 +0100
Subject: [PATCH] Allocate VT for the display

Replace the old crude algorithm to find the next available VT with a
more reliable method using the VT_OPENQRY ioctl.

General.MinimumVT setting is now obsolete and it's no longer used.
---
 CMakeLists.txt             |  4 ----
 data/man/sddm.conf.rst.in  |  2 +-
 src/common/Configuration.h |  1 -
 src/common/Constants.h.in  |  1 -
 src/daemon/Display.cpp     |  7 +++++--
 src/daemon/Display.h       |  2 +-
 src/daemon/Seat.cpp        | 38 +++-----------------------------------
 src/daemon/Seat.h          |  3 +--
 8 files changed, 11 insertions(+), 47 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index e52e0e903..9614b4e1e 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -141,7 +141,6 @@ if(SYSTEMD_FOUND)
         string(REGEX REPLACE "[ \t\n]+" "" SYSTEMD_SYSTEM_UNIT_DIR ${SYSTEMD_SYSTEM_UNIT_DIR})
     endif()
 
-    set(MINIMUM_VT 1)
     set(HALT_COMMAND "/usr/bin/systemctl poweroff")
     set(REBOOT_COMMAND "/usr/bin/systemctl reboot")
 else()
@@ -159,7 +158,6 @@ if(ELOGIND_FOUND)
     add_definitions(-DHAVE_ELOGIND)
     set(CMAKE_AUTOMOC_MOC_OPTIONS -DHAVE_ELOGIND)
 
-    set(MINIMUM_VT 7)
     set(HALT_COMMAND "/usr/bin/loginctl poweroff")
     set(REBOOT_COMMAND "/usr/bin/loginctl reboot")
 endif()
@@ -171,10 +169,8 @@ if (NOT ELOGIND_FOUND AND NOT SYSTEMD_FOUND)
     # commands for shutdown and reboot. On FreeBSD, there are
     # normally more getty's running than on Linux.
     if("${CMAKE_SYSTEM_NAME}" STREQUAL "FreeBSD")
-        set(MINIMUM_VT 9)
         set(HALT_COMMAND "/sbin/shutdown -p now")
     else()
-        set(MINIMUM_VT 7)
         set(HALT_COMMAND "/sbin/shutdown -h -P now")
     endif()
     set(REBOOT_COMMAND "/sbin/shutdown -r now")
diff --git a/data/man/sddm.conf.rst.in b/data/man/sddm.conf.rst.in
index bee07681d..1061540c0 100644
--- a/data/man/sddm.conf.rst.in
+++ b/data/man/sddm.conf.rst.in
@@ -144,7 +144,7 @@ OPTIONS
 	Minimum virtual terminal number that will be used
 	by the first display. Virtual terminal number will
 	increase as new displays added.
-	Default value is @MINIMUM_VT@.
+	This setting is no longer available since SDDM v0.20.
 
 `EnableHiDPI=`
 	Enables Qt's automatic HiDPI scaling.
diff --git a/src/common/Configuration.h b/src/common/Configuration.h
index cf44a629c..b79871988 100644
--- a/src/common/Configuration.h
+++ b/src/common/Configuration.h
@@ -70,7 +70,6 @@ namespace SDDM {
 	    Entry(UserAuthFile,        QString,     _S(".Xauthority"),                          _S("Path to the Xauthority file"));
             Entry(DisplayCommand,      QString,     _S(DATA_INSTALL_DIR "/scripts/Xsetup"),     _S("Path to a script to execute when starting the display server"));
             Entry(DisplayStopCommand,  QString,     _S(DATA_INSTALL_DIR "/scripts/Xstop"),      _S("Path to a script to execute when stopping the display server"));
-            Entry(MinimumVT,           int,         MINIMUM_VT,                                 _S("The lowest virtual terminal number that will be used."));
             Entry(EnableHiDPI,         bool,        false,                                      _S("Enable Qt's automatic high-DPI scaling"));
         );
 
diff --git a/src/common/Constants.h.in b/src/common/Constants.h.in
index e174b5bfc..480512880 100644
--- a/src/common/Constants.h.in
+++ b/src/common/Constants.h.in
@@ -37,7 +37,6 @@
 #define SYSTEM_CONFIG_DIR           "@SYSTEM_CONFIG_DIR@"
 
 #define LOG_FILE                    "@LOG_FILE@"
-#define MINIMUM_VT                  @MINIMUM_VT@
 
 #define UID_MIN                     @UID_MIN@
 #define UID_MAX                     @UID_MAX@
diff --git a/src/daemon/Display.cpp b/src/daemon/Display.cpp
index ed13bae79..3c77454fc 100644
--- a/src/daemon/Display.cpp
+++ b/src/daemon/Display.cpp
@@ -46,6 +46,7 @@
 
 #include "Login1Manager.h"
 #include "Login1Session.h"
+#include "VirtualTerminal.h"
 
 #if defined(Q_OS_LINUX)
 #include <utmp.h>
@@ -53,14 +54,16 @@
 #include <utmpx.h>
 
 namespace SDDM {
-    Display::Display(const int terminalId, Seat *parent) : QObject(parent),
-        m_terminalId(terminalId),
+    Display::Display(Seat *parent) : QObject(parent),
         m_auth(new Auth(this)),
         m_displayServer(new XorgDisplayServer(this)),
         m_seat(parent),
         m_socketServer(new SocketServer(this)),
         m_greeter(new Greeter(this)) {
 
+        // Allocate vt
+        m_terminalId = VirtualTerminal::setUpNewVt();
+
         // respond to authentication requests
         m_auth->setVerbose(true);
         connect(m_auth, &Auth::requestChanged, this, &Display::slotRequestChanged);
diff --git a/src/daemon/Display.h b/src/daemon/Display.h
index 8b3c2c635..61dd9f630 100644
--- a/src/daemon/Display.h
+++ b/src/daemon/Display.h
@@ -41,7 +41,7 @@ namespace SDDM {
         Q_OBJECT
         Q_DISABLE_COPY(Display)
     public:
-        explicit Display(int terminalId, Seat *parent);
+        explicit Display(Seat *parent);
         ~Display();
 
         QString displayId() const;
diff --git a/src/daemon/Seat.cpp b/src/daemon/Seat.cpp
index 838c2221d..a2f3d0c39 100644
--- a/src/daemon/Seat.cpp
+++ b/src/daemon/Seat.cpp
@@ -33,18 +33,6 @@
 #include <functional>
 
 namespace SDDM {
-    int findUnused(int minimum, std::function<bool(const int)> used) {
-        // initialize with minimum
-        int number = minimum;
-
-        // find unused
-        while (used(number))
-            number++;
-
-        // return number;
-        return number;
-    }
-
     Seat::Seat(const QString &name, QObject *parent) : QObject(parent), m_name(name) {
         createDisplay();
     }
@@ -53,30 +41,13 @@ namespace SDDM {
         return m_name;
     }
 
-    void Seat::createDisplay(int terminalId) {
+    void Seat::createDisplay() {
         //reload config if needed
         mainConfig.load();
 
-        if (m_name == QLatin1String("seat0")) {
-            if (terminalId == -1) {
-                // find unused terminal
-                terminalId = findUnused(mainConfig.X11.MinimumVT.get(), [&](const int number) {
-                    return m_terminalIds.contains(number);
-                });
-            }
-
-            // mark terminal as used
-            m_terminalIds << terminalId;
-
-            // log message
-            qDebug() << "Adding new display" << "on vt" << terminalId << "...";
-        }
-        else {
-            qDebug() << "Adding new VT-less display...";
-        }
-
         // create a new display
-        Display *display = new Display(terminalId, this);
+        qDebug() << "Adding new display...";
+        Display *display = new Display(this);
 
         // restart display on stop
         connect(display, &Display::stopped, this, &Seat::displayStopped);
@@ -112,9 +83,6 @@ namespace SDDM {
         // remove display from list
         m_displays.removeAll(display);
 
-        // mark display and terminal ids as unused
-        m_terminalIds.removeAll(display->terminalId());
-
         // stop the display
         display->blockSignals(true);
         display->stop();
diff --git a/src/daemon/Seat.h b/src/daemon/Seat.h
index f9fe7331f..685eaedd2 100644
--- a/src/daemon/Seat.h
+++ b/src/daemon/Seat.h
@@ -35,7 +35,7 @@ namespace SDDM {
         const QString &name() const;
 
     public slots:
-        void createDisplay(int terminalId = -1);
+        void createDisplay();
         void removeDisplay(SDDM::Display* display);
 
     private slots:
@@ -47,7 +47,6 @@ namespace SDDM {
         QString m_name;
 
         QVector<Display *> m_displays;
-        QVector<int> m_terminalIds;
     };
 }
 
