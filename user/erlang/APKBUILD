# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=24.3.3
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="autoconf automake flex libxml2-utils libxslt-dev m4 ncurses-dev
	openssl-dev perl unixodbc-dev"
subpackages="$pkgname-dev"
source="erlang-$pkgver.tar.gz::https://github.com/erlang/otp/archive/OTP-$pkgver.tar.gz"
builddir="$srcdir/otp-OTP-$pkgver"

build() {
	./otp_build autoconf
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads \
		--disable-hipe
	make
}

check() {
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h \
		erl_memory_trace_parser.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5e8ce0ebd50c1c67ca29ce9405fc18f5461793b20a1732418bc8fac404078044deb72526682aab92ee3e737bc9dc9e94e6be162a088e2cb7cc68c14b1d76454e  erlang-24.3.3.tar.gz"
