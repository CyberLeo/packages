# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=openjdk8
_icedteaver=3.21.0
# pkgver is <JDK version>.<JDK update>.<JDK build>
# Check https://icedtea.classpath.org/wiki/Main_Page when updating
pkgver=8.312.07
pkgrel=1
pkgdesc="Libre Java development kit for Java 8"
url="https://icedtea.classpath.org/"
arch="all !armv7"  #1007
options="sover-namecheck"
license="GPL-2.0-only"
depends="$pkgname-jre java-cacerts"
makedepends="bash findutils libarchive-tools zip file util-linux libxslt
	autoconf automake linux-headers sed xz coreutils
	fastjar ca-certificates libjpeg-turbo-dev cmd:which
	nss-dev nss-static cups-dev giflib-dev libpng-dev libxt-dev
	lcms2-dev libxp-dev libxtst-dev libxinerama-dev zlib-dev
	libxrender-dev alsa-lib-dev freetype-dev fontconfig-dev
	gtk+2.0-dev krb5-dev attr-dev pcsc-lite-dev lksctp-tools-dev
	libxcomposite-dev"

case $CARCH in
x86|pmmx)	_jarch=i386;;
x86_64)		_jarch=amd64;;
arm*)		_jarch=aarch32;;
ppc64)		_jarch=ppc64
		export CFLAGS="$CFLAGS -DABI_ELFv2";;
*)		_jarch="$CARCH";;
esac

_bootstrap_java_home="$srcdir/boot-home/$CARCH"
_java_home="/usr/lib/jvm/java-1.8-openjdk"
_jrelib="$_java_home/jre/lib/$_jarch"

# Exclude xawt from ldpath to avoid duplicate provides for libmawt.so
# (also in headless). in future this should be a virtual provides.
ldpath="$_jrelib:$_jrelib/native_threads:$_jrelib/headless:$_jrelib/server:$_jrelib/jli"
sonameprefix="$pkgname:"

provides="$pkgname-bootstrap=$pkgver-r$pkgrel"

subpackages="$pkgname-dbg $pkgname-jre-lib:jrelib:noarch $pkgname-jre $pkgname-jre-base:jrebase
	$pkgname-doc $pkgname-demos"

_dropsver=$_icedteaver
_dropsurl="https://icedtea.classpath.org/download/drops/icedtea8/$_dropsver"

source="https://icedtea.classpath.org/download/source/icedtea-$_icedteaver.tar.xz
	openjdk-$_dropsver.tar.xz::$_dropsurl/openjdk.tar.xz
	corba-$_dropsver.tar.xz::$_dropsurl/corba.tar.xz
	jaxp-$_dropsver.tar.xz::$_dropsurl/jaxp.tar.xz
	jaxws-$_dropsver.tar.xz::$_dropsurl/jaxws.tar.xz
	jdk-$_dropsver.tar.xz::$_dropsurl/jdk.tar.xz
	langtools-$_dropsver.tar.xz::$_dropsurl/langtools.tar.xz
	hotspot-$_dropsver.tar.xz::$_dropsurl/hotspot.tar.xz
	nashorn-$_dropsver.tar.xz::$_dropsurl/nashorn.tar.xz

	icedtea-hotspot-musl.patch
	icedtea-hotspot-musl-ppc.patch
	icedtea-hotspot-noagent-musl.patch
	icedtea-jdk-execinfo.patch
	icedtea-jdk-fix-ipv6-init.patch
	icedtea-jdk-fix-libjvm-load.patch
	icedtea-jdk-musl.patch
	icedtea-jdk-includes.patch
	icedtea-autoconf-config.patch
	remove-gawk.patch

	project-autoconf-2xx.patch
	"
_targets="aarch64 ppc64 ppc x86_64 pmmx"
for target in $_targets; do
	source="$source
		https://distfiles.adelielinux.org/source/openjdk/openjdk8-bootstrap-$target.txz
	"
done
builddir="$srcdir/icedtea-$_icedteaver"

# secfixes:
#   8.252.09-r0:
#     - CVE-2019-2602
#     - CVE-2019-2684
#     - CVE-2019-2698
#     - CVE-2019-2745
#     - CVE-2019-2762
#     - CVE-2019-2766
#     - CVE-2019-2769
#     - CVE-2019-2786
#     - CVE-2019-2816
#     - CVE-2019-2842
#     - CVE-2019-2894
#     - CVE-2019-2933
#     - CVE-2019-2945
#     - CVE-2019-2949
#     - CVE-2019-2958
#     - CVE-2019-2962
#     - CVE-2019-2964
#     - CVE-2019-2973
#     - CVE-2019-2975
#     - CVE-2019-2978
#     - CVE-2019-2981
#     - CVE-2019-2983
#     - CVE-2019-2987
#     - CVE-2019-2988
#     - CVE-2019-2989
#     - CVE-2019-2992
#     - CVE-2019-2999
#     - CVE-2019-7317
#     - CVE-2020-2583
#     - CVE-2020-2590
#     - CVE-2020-2593
#     - CVE-2020-2601
#     - CVE-2020-2604
#     - CVE-2020-2654
#     - CVE-2020-2659
#     - CVE-2020-2754
#     - CVE-2020-2755
#     - CVE-2020-2756
#     - CVE-2020-2757
#     - CVE-2020-2773
#     - CVE-2020-2781
#     - CVE-2020-2800
#     - CVE-2020-2803
#     - CVE-2020-2805
#     - CVE-2020-2830
#   8.201.08-r0:
#     - CVE-2019-2422
#     - CVE-2019-2426
#     - CVE-2018-11212
#   8.191.12-r0:
#     - CVE-2018-3136
#     - CVE-2018-3139
#     - CVE-2018-3149
#     - CVE-2018-3169
#     - CVE-2018-3180
#     - CVE-2018-3183
#     - CVE-2018-3214
#     - CVE-2018-13785
#     - CVE-2018-16435
#   8.181.13-r0:
#     - CVE-2018-2938
#     - CVE-2018-2940
#     - CVE-2018-2952
#     - CVE-2018-2973
#     - CVE-2018-3639

unpack() {
	if [ -z "$force" ]; then
		verify
		initdcheck
	fi
	mkdir -p "$srcdir"
	msg "Unpacking sources..."
	tar -C "$srcdir" -xJf icedtea-$_icedteaver.tar.xz
	tar -C "$srcdir" -xJf openjdk8-bootstrap-$CARCH.txz \
		|| die "Architecture $CARCH not bootstrapped"
}

prepare() {
	_ver_u="$(sed -En 's/^\s*JDK_UPDATE_VERSION\s*=\s*(\S+).*/\1/p' acinclude.m4)"
	_ver_b="$(sed -En 's/^\s*BUILD_VERSION\s*=\s*b(\S+).*/\1/p' acinclude.m4)"
	[ "${pkgver#*.}" = "$_ver_u.$_ver_b" ] \
		|| die "Version mismatch, source is 8.$_ver_u.$_ver_b, but abuild defines $pkgver!"

	# Busybox sha256 does not support longopts.
	sed -e "s/--check/-c/g" -i Makefile.am

	for _patch in $source; do
		case $_patch in
		icedtea-*.patch)
			cp ../$_patch patches
			;;
		*.patch)
			msg "Applying patch $_patch"
			patch -p1 -i "$srcdir"/$_patch
			;;
		esac
	done

	./autogen.sh
}

build() {
	export JAVA_HOME="$_bootstrap_java_home"
	export PATH="$JAVA_HOME/bin:$PATH"

	if [ -z "$JOBS" ]; then
		export JOBS=$(printf '%s\n' "$MAKEFLAGS" | sed -n -e 's/.*-j\([0-9]\+\).*/\1/p')
	fi

	DISTRIBUTION_PATCHES=""
	for _patch in $source; do
		case $_patch in
		icedtea-*.patch)
			DISTRIBUTION_PATCHES="$DISTRIBUTION_PATCHES patches/$_patch"
			;;
		esac
	done
	export DISTRIBUTION_PATCHES
	echo "icedtea patches: $DISTRIBUTION_PATCHES"

	bash ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix="$_java_home" \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-dependency-tracking \
		--disable-downloading \
		--disable-precompiled-headers \
		--with-parallel-jobs=${JOBS:-2} \
		--with-hotspot-build=default \
		--with-openjdk-src-zip="$srcdir/openjdk-$_dropsver.tar.xz" \
		--with-hotspot-src-zip="$srcdir/hotspot-$_dropsver.tar.xz" \
		--with-corba-src-zip="$srcdir/corba-$_dropsver.tar.xz" \
		--with-jaxp-src-zip="$srcdir/jaxp-$_dropsver.tar.xz" \
		--with-jaxws-src-zip="$srcdir/jaxws-$_dropsver.tar.xz" \
		--with-jdk-src-zip="$srcdir/jdk-$_dropsver.tar.xz" \
		--with-langtools-src-zip="$srcdir/langtools-$_dropsver.tar.xz" \
		--with-nashorn-src-zip="$srcdir/nashorn-$_dropsver.tar.xz" \
		--with-jdk-home="$_bootstrap_java_home" \
		--with-pkgversion="Adelie ${pkgver}-r${pkgrel}" \
		--enable-nss \
		--enable-non-nss-curves \
		--disable-jfr `# configure error without this` \
		;
	make
}

# TODO: Run tests or at least try to compile and run hello world.
check() {
	cd "$builddir"/openjdk.build/images/j2sdk-image

	./bin/java -version
}

package() {
	mkdir -p "$pkgdir"/$_java_home

	cp -a openjdk.build/images/j2sdk-image/* "$pkgdir"/$_java_home/
	rm "$pkgdir"/$_java_home/src.zip

	# This archive contains absolute paths from the build environment,
	# so it does not work on the target system. User can generate it
	# running `java -Xshare:dump`.
	rm -f "$pkgdir"/$_jrelib/server/classes.jsa

	# symlink to shared java cacerts store
	rm -f "$pkgdir"/$_java_home/jre/lib/security/cacerts
	ln -sf /etc/ssl/certs/java/cacerts \
		"$pkgdir"/$_java_home/jre/lib/security/cacerts
}

jrelib() {
	pkgdesc="OpenJDK 8 Java Runtime (class libraries)"
	depends=""

	for _file in jre/lib/images \
			jre/lib/*.jar \
			jre/lib/security \
			jre/lib/ext/*.jar \
			jre/lib/cmm \
			jre/ASSEMBLY_EXCEPTION \
			jre/THIRD_PARTY_README \
			jre/LICENSE; do

		_dir=${_file%/*}
		mkdir -p "$subpkgdir"/$_java_home/$_dir
		mv "$pkgdir"/$_java_home/$_file "$subpkgdir"/$_java_home/$_dir
	done
}

jre() {
	pkgdesc="OpenJDK 8 Java Runtime"

	mkdir -p "$subpkgdir"
	for _file in jre/bin/policytool \
			bin/appletviewer \
			bin/policytool \
			jre/lib/$_jarch/libawt_xawt.so \
			jre/lib/$_jarch/libfontmanager.so \
			jre/lib/$_jarch/libjawt.so \
			jre/lib/$_jarch/libjsoundalsa.so \
			jre/lib/$_jarch/libsplashscreen.so; do

		_dir=${_file%/*}
		mkdir -p "$subpkgdir"/$_java_home/$_dir
		mv "$pkgdir"/$_java_home/$_file "$subpkgdir"/$_java_home/$_dir
	done
}

jrebase() {
	pkgdesc="OpenJDK 8 Java Runtime (no GUI support)"
	depends="$pkgname-jre-lib java-common java-cacerts"

	mkdir -p "$subpkgdir"/$_java_home/bin \
		"$subpkgdir"/$_java_home/lib/$_jarch

	mv "$pkgdir"/$_java_home/lib/$_jarch/jli \
		"$subpkgdir"/$_java_home/lib/$_jarch/

	for _file in java orbd rmid servertool unpack200 keytool \
			pack200 rmiregistry tnameserv; do
		mv "$pkgdir"/$_java_home/bin/$_file "$subpkgdir"/$_java_home/bin/
	done

	# Rest of the jre subdir (which were not taken by -jre subpkg).
	mv "$pkgdir"/$_java_home/jre "$subpkgdir"/$_java_home/
}

doc() {
	default_doc

	mkdir -p "$subpkgdir"/$_java_home/
	mv "$pkgdir"/$_java_home/man "$subpkgdir"/$_java_home/
}

demos() {
	pkgdesc="OpenJDK 8 Java Demos and Samples"
	depends="$pkgname"

	mkdir -p "$subpkgdir"/$_java_home/
	mv "$pkgdir"/$_java_home/demo "$pkgdir"/$_java_home/sample \
		"$subpkgdir"/$_java_home/
}

sha512sums="874f91f3c3311d7017efb91aaff4e2fb660f55c58a626bcf5b120b7158df15e9b94abda7cd28f9620ad76c50bfa89cc8c1d251c193404c0db51ff1430167b018  icedtea-3.21.0.tar.xz
6a1706dfcb5e5d79191c01db6da30ca0ca48cea628a26a3a35f6e43c710091e25c97ec95122e8bc19b4ac69ede27a8dae37b49e1a7e4db07a635ded69e2e0336  openjdk-3.21.0.tar.xz
55110b92c6eddeb29eab97cbe644d8cc171a489b9fa806c81e4af88a1e5e3e6776fb97371715782935222944464f68e7d6783c46bd0d0de5659123f46c577a7a  corba-3.21.0.tar.xz
ff14c9b63cee2a7b9213872b8710b1c5b901ccaf4122c8ddf5d8437d1cdd35cf9af50f461d05f004b74802ba8a21c7e9f288ec080b2b5f7daad065d40dc39258  jaxp-3.21.0.tar.xz
e21bd36b6cfa92f648856532647150243f2184943dc6f5003fea02cf69aa9b6a94fb18d6478db28d5ee5e3af9b780122c0539fd3207cf4dfcb69a042835c13d6  jaxws-3.21.0.tar.xz
06385f14d093be250aa530d0659576d75aed57b0264ec7329d6b2c9309cf4ed660a9ecbfb6f199868ae23aedfbdb97036fccda501c00e9d2e3b1562c92b0ed37  jdk-3.21.0.tar.xz
949366138278104fc06452d8227bbe5d0829a9124503928cab34e5e9df37e25d25964a5c755e46d9ca74e63f1b318b2e3366f8f08e144524a8043bc2a7ca626e  langtools-3.21.0.tar.xz
678f6c53db5858ba828e063eeeddb727dbeb0586ff26a4287f9735dc01ad00ddcd9f41fa65343b81d5f5c512a944f47a447d4bdc929ca26ba78a68a7e3dfc1cd  hotspot-3.21.0.tar.xz
eda17f94ed2f34dd5bc4670dd3340d50de956723213948fd15963b0a589433bbe2e0ba8be7761974754b6a5d875ef58c31839daa6fef7215af552842f787bb44  nashorn-3.21.0.tar.xz
bfbeccc931b9eab04fca94167b7569af26195297130e2effd9175d33b74dec3dc5727fea6e0cbf3cce21ba09641ddd868179544d3fabe8b128baaaccb9c2711c  icedtea-hotspot-musl.patch
86e77c1e5e8a48f121e608dce5eafad7a714e4029b55dbd554c2c94633b49a4239f71a40a41273b54d62fcdcfdee21340c8b85f96001cf15b719b02a520e8d9a  icedtea-hotspot-musl-ppc.patch
19459dbb922f5a71cd15b53199481498626a783c24f91d2544d55b7dddd2cdb34a64bbf0226b99548612dd1743af01b3f9ff32c30abbbc90ce727ca2dbbbd1f9  icedtea-hotspot-noagent-musl.patch
f6365cfafafa008bd6c1bf0ccec01a63f8a39bd1a8bc87baa492a27234d47793ba02d455e5667a873ef50148df3baaf6a8421e2da0b15faac675867da714dd5f  icedtea-jdk-execinfo.patch
48533f87fc2cf29d26b259be0df51087d2fe5b252e72d00c6ea2f4add7b0fb113141718c116279c5905e03f64a1118082e719393786811367cf4d472b5d36774  icedtea-jdk-fix-ipv6-init.patch
b135991c76b0db8fa7c363e0903624668e11eda7b54a943035c214aa4d7fc8c3e8110ed200edcec82792f3c9393150a9bd628625ddf7f3e55720ff163fbbb471  icedtea-jdk-fix-libjvm-load.patch
17c78db081a85e37721c23e0c0e7cab85e2201a0969bd4858cb90375b97d1703c9bf867f8ac02f6b33f9775b78bae41e38223b7a887918d4a6c9f29b75f3de28  icedtea-jdk-musl.patch
974fb54532b7e7d738f4278187fc6bd9f9b2d99866b94f68a617ee4911c89a3b8cc41ecfdcaefecf9157492d006b1844b6b0b41ac4209d84f9e8d13c9e485dd3  icedtea-jdk-includes.patch
662d662d0a7a84be2978e921317589f212f3ba3b7629527ba0f1140b5ac4c1024893e0ed176211688ed1a4505968c4befc841ed57ffcdbb9d355c2cb0571b167  icedtea-autoconf-config.patch
b0f6d07c6a949acdc8b4a25bf924f134f468e162f01dd440fd4ca80769fb84a0a54210f93efbe88012404fe3db6701aad31cdbc772bc054ad69021c37db5538c  remove-gawk.patch
6dd39baca14f088d0b2157308e3d9657ab64ecdb7daab09137486e212e3a8c53f6deae6167b6212217c6ef29c1406347e21ca6bb193ced9ebeb399aeb06bda4b  project-autoconf-2xx.patch
2bbf44ce9145554829c9645e12645fed671201a2af73891d0552295a0a38d45d2d98dccf7db888926450481537e9aebaf4fe12701494e432af10b3ed79bf524e  openjdk8-bootstrap-aarch64.txz
2c0aa356ad015e4176250eda49060f73fef85e8cd457144ab131c03ff79acc5721062755c60b0efa8058602244e2c4f79ec0985e219ff37a4fc933c485a070ce  openjdk8-bootstrap-ppc64.txz
c4c5d65b41a5928b0583c5a7a82530f802380d6104dfa60c0613682e29193760505ca06427a30613d72a9d45609038e5192440f799d348ce89f213ede0659d4a  openjdk8-bootstrap-ppc.txz
c4e6e620f43a9b71e06a616853abce95452044147ca26ac2f484d1e4e4c23f03682ad650a77968f96d88a554832b341f6aa6e7cb059ae87f99349fe8d68a5d6c  openjdk8-bootstrap-x86_64.txz
9281ac1dee1bf35fbdb320cac615de1bfd731853cc1ade59896341f28a2d126f3106fffcdda25375e8c96d224cbe60962e6862893bfc7fff1fe2efbf3900e06c  openjdk8-bootstrap-pmmx.txz"
