# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=libpng
pkgver=1.6.39
pkgrel=0
pkgdesc="Portable Network Graphics library"
url="http://www.libpng.org/pub/png/libpng.html"
arch="all"
license="Libpng"
depends=""
depends_dev=""
makedepends="autoconf automake libtool zlib-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-utils"
source="https://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tar.gz
	https://downloads.sourceforge.net/sourceforge/$pkgname-apng/$pkgname-$pkgver-apng.patch.gz
	libpng-fix-arm-neon.patch
	"

prepare() {
	gunzip -c "$srcdir"/$pkgname-$pkgver-apng.patch.gz | patch -p1
	default_prepare
	# libpng-fix-arm-neon.patch modifies configure.ac
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="$pkgdesc (pngfix utils)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="19851afffbe2ffde62d918f7e9017dec778a7ce9c60c75cdc65072f086e6cdc9d9895eb7b207535a84cb5f4ead77ebc2aa9d80025f153662903023e1f7ab9bae  libpng-1.6.39.tar.gz
97a182da0b3b54aecf735e3655d8e8f1a569ae957b23fc3d7a9c8cc65dcdd26f34f173ce9f60af99b01d5347267b2afefaf787c500ce1005e86bf2810b3d0738  libpng-1.6.39-apng.patch.gz
0a5efa609bda4c2a38f5110b7287020a5f9dc81583f2b05a951d897a18ce62ea80350baf52daa3d02f20dff51bbc177b3af317ab7bbc09eb23a3c252600414a5  libpng-fix-arm-neon.patch"
