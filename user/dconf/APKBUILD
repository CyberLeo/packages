# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=dconf
pkgver=0.40.0
pkgrel=1
pkgdesc="Configuration management for the Gnome desktop environment"
url="https://gnome.org"
arch="all"
options="!check"  # Requires running D-Bus daemon.
license="LGPL-2.0+ AND LGPL-2.1+"
depends="dbus"
makedepends="meson ninja
	bash-completion dbus-dev docbook-xsl libxslt-dev python3-dev vala-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.gnome.org/sources/dconf/${pkgver%.*}/dconf-$pkgver.tar.xz
	user-profile.conf"

build() {
	meson \
		-Dprefix=/usr \
		--buildtype=release \
		. build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
	install -Dm644 "$srcdir"/user-profile.conf "$pkgdir"/etc/dconf/profile/user
}

sha512sums="71396d71f24f47653181482b052fdfc63795c50c373de34e2fb93e16101745daa7e81192b79a102d5389911cea34138eedf3ac32bc80562018e8a7f31963559a  dconf-0.40.0.tar.xz
226ef29cced7271d503ae29580fa189994067aa174903844b1bcc619d1e33e6038e5d9d3dd6d502505fdb966489605227323cba37db2d5287b6317ad2d31480e  user-profile.conf"
