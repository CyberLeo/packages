# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=protobuf
_gemname=google-protobuf
pkgver=3.20.3
_tstver=8d51dc50eb7e7698427fed81b85edad0e032112e
pkgrel=1
pkgdesc="Library for extensible, efficient structure packing"
url="https://github.com/google/protobuf"
arch="all"
options="!check"  # Broken everywhere.
license="BSD-3-Clause"
depends_dev="zlib-dev"
makedepends="$depends_dev autoconf automake libtool ruby ruby-dev ruby-rake
	ruby-rake-compiler ruby-io-console ruby-irb ruby-power_assert 
	ruby-rake-compiler ruby-test-unit ruby-reline ruby-rubygems-tasks
	python3 python3-dev cmake"
checkdepends="ruby-json ruby-test-unit"
subpackages="ruby-$_gemname:_ruby py3-$pkgname:_python $pkgname-dev $pkgname-vim::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/google/$pkgname/archive/v$pkgver.tar.gz
	googletest-$_tstver.tar.gz::https://github.com/google/googletest/archive/$_tstver.tar.gz
	cxx14.patch
	32bit.patch
	python-311.patch
	"

prepare() {
	default_prepare

	# symlink tests to the test directory
	rm -rf third_party/googletest
	ln -sf "$srcdir"/googletest-$_tstver \
		"$builddir"/third_party/googletest
}

build() {
	# Build Protobuf
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} cmake
	make -j1

	# Build for Ruby
	cd "$builddir"/ruby
	# Generate proto files for built-in protocols.
	export LD_LIBRARY_PATH=${builddir}
	export CPPFLAGS="-L${builddir}"
	export PATH="${builddir}:$PATH"
	rake
	gem build $_gemname.gemspec
	gem install --local \
		--install-dir dist \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Build for Python 3
	cd "$builddir"/python
	python3 setup.py build --cpp_implementation

	# Build test-suite
	cd "$builddir"/third_party/googletest/
	cmake .
	make -j1
}

check() {
	make check
	cd "$builddir"/ruby
	rake test
	cd "$builddir"/python
	python3 setup.py test --cpp_implementation
}

package() {
	make DESTDIR="$pkgdir" install
}

_ruby() {
	pkgdesc="Ruby bindings to Google's data interchange format"

	local gemdir="$subpkgdir/$(ruby -e 'puts Gem.default_dir')"
	cd "$builddir"/ruby/dist

	mkdir -p "$gemdir"
	cp -r extensions gems specifications "$gemdir"/

	# Remove duplicated .so libs (should be only in extensions directory).
	find "$gemdir"/gems/ -name "*.so" -delete

	# Remove unnecessary files.
	cd "$gemdir"/gems/$_gemname-$pkgver
	rm -r ext/ tests/
}

_python() {
	pkgdesc="Python bindings to Google's data interchange format"

	cd "$builddir"/python
	python3 setup.py install --prefix=/usr --root="$subpkgdir" \
		--cpp_implementation
}

vim() {
	pkgdesc="Vim syntax for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel vim"

	install -Dm644 "$builddir"/editors/proto.vim \
		"$subpkgdir"/usr/share/vim/vimfiles/syntax/proto.vim
}

sha512sums="01d6703bdbe769a1200ee6e4ebcdcb99688ec21f576988c60d82ec36e0822820fb245fcb4ca53293143d53e666d748b5a0c6937bc659fb3cdc4cd9b05ed12a1c  protobuf-3.20.3.tar.gz
f7f804abf68af5e4e6cd767151773394fb8297d7d7fc878532ebb22b8c41d13554f68fa38a27470d458b590259a939e93cee7e5f5f6de8f1726c7ce85a606099  googletest-8d51dc50eb7e7698427fed81b85edad0e032112e.tar.gz
faf8962f0c7f1e4053b28a712b31ac9b254b17986326d2188d6edcc609f4b52f4cb85766c4bc02c6b9bc7bc30e2061f940a3089db54eb6056f729a80c1cfa891  cxx14.patch
2dfb540395460f0ea9fad663851633b29fa368a6ec99a88a42e31d7547034191679ef868c0ec67613d070659d4e322dc942b54b21793764e3d2342927977c7eb  32bit.patch
acb8f0bfec92ff969699c909e509f28c1b8c95acb739ce9c3fedaa3f961e2b799190d9eeb4c6022b7cc9244aa7e6c47640cec077dff9382960ea3fc65b24e906  python-311.patch"
