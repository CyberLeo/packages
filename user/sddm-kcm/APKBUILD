# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=sddm-kcm
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE configuration applet for SDDM"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qtdeclarative-dev kcoreaddons-dev ki18n-dev kxmlgui-dev kauth-dev
	kconfigwidgets-dev kio-dev libxcb-dev xcb-util-image-dev libxcursor-dev
	knewstuff-dev kcmutils-dev kdeclarative-dev"
install_if="systemsettings sddm"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1fbc050e70dde74bcaa0a97ac982f017146ca65f180ea07e57190bd13ea653789fb369b83bc0b2583b73d37e4cd3db087eebe2cdaa3346b81c1d7d11730fad15  sddm-kcm-5.24.5.tar.xz"
