# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=lsof
pkgver=4.93.2
pkgrel=0
pkgdesc="LiSt Open Files"
url="https://people.freebsd.org/~abe"
arch="all"
license="zlib-acknowledgement"
depends=""
makedepends="linux-headers cmd:which utmps-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/lsof-org/lsof/archive/$pkgver.tar.gz
	utmps.patch
	test-disable-nfs-test.patch
	test-enable-suite.patch
	test-linux-sysmacros.patch
	"

prepare() {
	default_prepare

	sed -i "s|/\* #define\tHASSECURITY\t1 \*/|#define\tHASSECURITY\t1|" \
		dialects/linux/machine.h
	sed -n 6,30p lsof.h > COPYING
}

build () {
	./Configure -n linux
	make
}

check() {
	# The LTsock test will fail if /etc/hosts is not set correctly.
	# Make sure that 127.0.0.1 and ::1 have aliases corresponding to
	# `hostname`.
	sed -i tests/CkTestDB \
		-e 's/^# This dialect may never have been validated.*$/exit 0/'
	make -C tests
	make -C tests opt
}

package() {
	install -Dm755 lsof "$pkgdir/usr/bin/lsof"
	install -Dm644 Lsof.8 "$pkgdir/usr/share/man/man8/lsof.8"
	install -Dm644 COPYING "$pkgdir/usr/share/licenses/lsof/COPYING"
}

sha512sums="3c4a4d503391b1a648a08a4947c2e72a9629b169eddf50738b16a16a82f2a26967e4db2902e4a9157de5a73ae638db48e392117ffbd591e9fafd6b1725cd01a0  lsof-4.93.2.tar.gz
d6f2f5b991c137abf30b8cd29eebaf7662f7255e6d99b7fba5c945eba09a2c7ce7c444895e72c6871a7d90fe3ad8ca28b052a428127ba0342b13b3b027a3c77f  utmps.patch
f2128d3aa3183244ade8a553f5a1ca7a5db571eae4ebd9c87c05f4b2996de152796573dd35d9c57d0f35671f2eb928b66fe637b1693ec1b8828c9a8d739c6771  test-disable-nfs-test.patch
72923d42db63435a95b07d17b6c6e8ac64f1a8f20505f851ed706783078bce0c49cac61dd8c05c08d24333c07ad981aaf34d4a4e4d1edb2a82ae8c9e9b8328e0  test-enable-suite.patch
14473839fa695e84cddac8bc1a33e06aba082bcc56baa6c4a6134a6c55ec1c9d08f756dab483785c0796a9442345853f7762fed76efb60a732fbc709bb06d25f  test-linux-sysmacros.patch"
