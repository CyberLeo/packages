#!/bin/sh -e
# Usage: davmail [</path/to/davmail.properties>]

# force GTK2 to avoid crash with OpenJDK 11
JAVA_OPTS="-Xmx512M -Dsun.net.inetaddr.ttl=60 -Djdk.gtk.version=2.2"
JAVA_CP="/usr/lib/java/davmail/lib/*"
exec java $JAVA_OPTS -cp "$JAVA_CP" davmail.DavGateway "$@"
